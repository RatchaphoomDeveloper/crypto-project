import { Component } from "react";
import axios from 'axios'
class Api extends Component {
  static getSocket = async () => {
    const result = await axios({
      url: "/socket_price",
      method: "get",
    }).catch((err) => {
      // MessageStaus.errresmessage(err.response.data.Message);
    });
    return result.data;
  };
}

export default Api;
