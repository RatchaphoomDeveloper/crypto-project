import React from "react";
import { Switch, Route, NavLink } from "react-router-dom";
import Main from "../Main/Main";
import { Buttons } from "../../Utils/Buttons/Buttons";
import Register from '../Register/Register'
import Login from '../Login/Login'
import ResetPassword from '../ResetPassword/ResetPassword'
import "./NavBar.scss";
import { useHistory } from "react-router-dom";
import ChartProfile from "../Chart/ChartProfile";
const NavBar = () => {
  const history = useHistory()
  const [clicked, setClicked] = React.useState(false);
  return (
    <div>
      <nav className="NavbarItems">
        <h1 className="navbar-logo">CRYPTO PLATFORM  <span className="fas fa-th"></span></h1>

        <div
          className="menu-icon"
          onClick={() => {
            setClicked(!clicked);
          }}
        >
          <i className={clicked ? "fas fa-times" : "fas fa-bars"}></i>
        </div>
        {/* <div className="dropdown">
          <span className="fas fa-th"></span>
          <div className="dropdown-content">
            <p>Hello World!</p>
            <p>Hello World!</p>
          </div>
        </div> */}
        
        <ul className={clicked ? "nav-menu active" : "nav-menu"}>
          <li>
            <NavLink to="/" className="nav-links">
              หน้าหลัก
            </NavLink>
          </li>
          <li>
            <NavLink to="/login" className="nav-links">
              Login
            </NavLink>
          </li>
          <li>
            <Buttons onClick={()=>{history.push("/register")}}>Register</Buttons>
          </li>
          <li>
            <NavLink to="#" className="nav-links">
              Download
            </NavLink>
          </li>
          <li>
            <div id="type-currency">
              <NavLink to="#" className="nav-links">
                USD
              </NavLink>
              <span id="span">|</span>
              <NavLink to="#" className="nav-links">
                THB
              </NavLink>
            </div>
          </li>
        </ul>
      </nav>
      <main>
        <Switch>
          <Route exact path="/" component={Main} />
          <Route exact path="/register" component={Register} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/reset_password" component={ResetPassword}  />
          <Route exact path="/chart_profile" component={ChartProfile}  />
        </Switch>
      </main>
    </div>
  );
};

export default NavBar;
