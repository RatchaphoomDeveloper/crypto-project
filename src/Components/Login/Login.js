import React from "react";
import { BiLock } from "react-icons/bi";
import { useHistory } from "react-router-dom";
import { countryCode } from "../../Models/CountryCode";
import "./Login.scss";
const Login = () => {
  const history = useHistory()
  const [tabActive1, setTabActive1] = React.useState(true);
  const [tabActive2, setTabActive2] = React.useState(false);
  const [eyeCheck1, seteyeCheck1] = React.useState(false);
  return (
    <div>
      <div className="login-containers">
        <div className="login-container">
          <h2>Login</h2>
          <p>Please check that you are visiting the correct URL</p>
          <div className="border-curve-login">
            <span>
              <BiLock className="bilock" />
              <label>https://accounts.binance.com</label>
            </span>
          </div>
          <div className="tab-login-container">
            <div
              className={tabActive1 === true ? "email-tab active" : "email-tab"}
              onClick={() => {
                setTabActive1(true);
                setTabActive2(false);
              }}
            >
              <label>Email</label>
            </div>
            <div
              className={
                tabActive2 === true ? "mobile-tab active" : "mobile-tab"
              }
              onClick={() => {
                setTabActive1(false);
                setTabActive2(true);
              }}
            >
              <label>Mobile</label>
            </div>
          </div>

          {tabActive1 === true && (
            <div className="login-tab-animation">
              <div className="login-tab-body">
                <div className="login-tab-form-group">
                  <label>Email</label>
                  <br />
                  <input type="text" />
                </div>
              </div>
              <div className="login-tab-body">
                <div className="login-tab-form-password-group">
                  <label>Password</label>
                  <br />
                  <input type={eyeCheck1 === false ? "password" : "text"} />
                  <span
                    className={
                      eyeCheck1 === false ? "far fa-eye" : "far fa-eye-slash"
                    }
                    onClick={() => {
                      seteyeCheck1(!eyeCheck1);
                    }}
                  ></span>
                </div>
                <button onClick={()=>{history.push('/chart_profile')}}>Login</button>
                <div className="login-tab-form-footer-group">
                  <div className="forgot" onClick={()=>{history.push('/reset_password')}}>
                    <label>Forgot Password?</label>
                  </div>
                  <div className="scanning">
                    <label id="scantologin">Scan to login</label>
                    <label>Free registeration</label>
                  </div>
                </div>
              </div>
            </div>
          )}
          {tabActive2 === true && (
            <div>
              <div className="login-mobile-container">
                <div className="mobile-phone-form-group">
                  <label>Mobile</label>
                  <div className="mobile-phone-flex-container">
                    <select>
                      {countryCode[0] &&
                        countryCode.map((res, i) => {
                          return (
                            <option value={res.dial_code}>
                              {res.dial_code}
                            </option>
                          );
                        })}
                    </select>
                    <input type="text" />
                  </div>
                </div>
                <div className="login-tab-form-password-group">
                  <label>Password</label>
                  <br />
                  <input type={eyeCheck1 === false ? "password" : "text"} />
                  <span
                    className={
                      eyeCheck1 === false ? "far fa-eye" : "far fa-eye-slash"
                    }
                    onClick={() => {
                      seteyeCheck1(!eyeCheck1);
                    }}
                  ></span>
                </div>
                <button id="button-login" onClick={()=>{history.push('/chart_profile')}}>Login</button>
                <div className="login-tab-form-footer-group">
                  <div className="forgot" onClick={()=>{history.push('/reset_password')}} >
                    <label>Forgot Password?</label>
                  </div>
                  <div className="scanning">
                    <label id="scantologin">Scan to login</label>
                    <label>Free registeration</label>
                  </div>
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default Login;
