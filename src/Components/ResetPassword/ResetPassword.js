import React from "react";
import { BiLock } from "react-icons/bi";
import "./ResetPassword.scss";
import "../Login/Login.scss";
import { countryCode } from "../../Models/CountryCode";
const ResetPassword = () => {
  const [tabActive1, setTabActive1] = React.useState(false);
  const [tabActive2, setTabActive2] = React.useState(true);
  const [eyeCheck1, seteyeCheck1] = React.useState(false);
  return (
    <div>
      <div className="login-containers">
        <div className="login-container">
          <h2>Reset Login Password</h2>
          <p id="reset-border-span"> <span id="fa-exclamation-circle" class="fas fa-exclamation-circle"></span> For security purposes, no withdrawals are permitted for 24 hours after modification of security methods.</p>
          <div className="tab-login-container">
            <div
              className={tabActive1 === true ? "email-tab active" : "email-tab"}
              onClick={() => {
                setTabActive1(true);
                setTabActive2(false);
              }}
            >
              <label>Email</label>
            </div>
            <div
              className={
                tabActive2 === true ? "mobile-tab active" : "mobile-tab"
              }
              onClick={() => {
                setTabActive1(false);
                setTabActive2(true);
              }}
            >
              <label>Mobile</label>
            </div>
          </div>

          {tabActive1 === true && (
            <div className="login-tab-animation">
              <div className="login-tab-body">
                <div className="login-tab-form-group">
                  <label>Email</label>
                  <br />
                  <input type="text" />
                </div>
              </div>
              <div className="login-tab-body">
                <button>Next</button>
              </div>
            </div>
          )}
          {tabActive2 === true && (
            <div>
              <div className="login-mobile-container">
                <div className="mobile-phone-form-group">
                  <label>Mobile</label>
                  <div className="mobile-phone-flex-container">
                    <select>
                      {countryCode[0] &&
                        countryCode.map((res, i) => {
                          return (
                            <option value={res.dial_code}>
                              {res.dial_code}
                            </option>
                          );
                        })}
                    </select>
                    <input type="text" />
                  </div>
                </div>
                <button id="button-login">Next</button>
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default ResetPassword;
