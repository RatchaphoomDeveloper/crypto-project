import React from 'react'
import {HashRouter} from 'react-router-dom'
import NavBar from '../src/Components/NavBar/NavBar' 
const Routers = () => {
    return (
        <div>
            <HashRouter basename="/" >
                <NavBar/>
            </HashRouter>
        </div>
    )
}

export default Routers
